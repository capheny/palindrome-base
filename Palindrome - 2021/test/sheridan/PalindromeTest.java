package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue(Palindrome.isPalindrome("racecar"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse(Palindrome.isPalindrome("abcca"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue(Palindrome.isPalindrome("Race car"));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse(Palindrome.isPalindrome("Race a car"));
	}	
	
}

